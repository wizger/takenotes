import smtplib
import zipfile
import tempfile
from email import encoders
from email.message import Message
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart    


from email.MIMEText import MIMEText




import os.path
class Close:

	def send_email(self,user, pwd, recipient, subject, body):
		try:
			fromaddr = user
			toaddr = recipient
			msg = MIMEMultipart()
			 
			msg['From'] = fromaddr
			#msg['To'] = toaddr
			msg['To'] = "," . join(toaddr)
			msg['Subject'] = subject
			 
			body = body
			msg.attach(MIMEText(body, 'plain'))
			 
			filename = "Clase.zip"#cambiar a donde va a estar el zip
			attachment = open(filename, "rb")
			part = MIMEBase('application', 'octet-stream')
			part.set_payload((attachment).read())
			encoders.encode_base64(part)
			part.add_header('Content-Disposition', "attachment; filename= %s" % filename)
			msg.attach(part)
			 
			server = smtplib.SMTP('smtp.gmail.com', 587)
			server.starttls()
			server.login(fromaddr, pwd)
			text = msg.as_string()
			server.sendmail(fromaddr, toaddr	, text)
			server.quit()			
		except:
			print("Ocurrio un error en Close: send_email")


	def zip(self,path, ziph):
	    # ziph is zipfile handle
	    for root, dirs, files in os.walk(path):
	        for file in files:
	            ziph.write(os.path.join(root, file))

	def send_file_zipped(self,the_file, recipients, sender='you@you.com'):
	    zf = tempfile.TemporaryFile(prefix='mail', suffix='.zip')
	    zip = zipfile.ZipFile(zf, 'w')
	    zip.write(the_file)
	    zip.close()
	    zf.seek(0)

	    # Create the message
	    themsg = MIMEMultipart()
	    themsg['Subject'] = 'File %s' % the_file
	    themsg['To'] = ', '.join(recipients)
	    themsg['From'] = sender
	    themsg.preamble = 'I am not using a MIME-aware mail reader.\n'
	    msg = MIMEBase('application', 'zip')
	    msg.set_payload(zf.read())
	    encoders.encode_base64(msg)
	    msg.add_header('Content-Disposition', 'attachment', 
	                   filename=the_file + '.zip')
	    themsg.attach(msg)
	    themsg = themsg.as_string()

	    # send the message
	    smtp = smtplib.SMTP()
	    smtp.connect()
	    smtp.sendmail(sender, recipients, themsg)
	    smtp.close()