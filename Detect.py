"""Autor: Gerson Ivan Perez Dominguez
12002764"""
import numpy as np
import cv2
import os.path
class Detect:
	#Devuelve una lista x1:y1:x2:y2 donde x1:y1 es la esquina superior derecha y x2:y2 es la esquina inferior derecha
	def Persona(self,foto):		
		bordes = np.arange(4)
		rangoDerX = 0		
		rangoDerY = 0
		rangoIzX = 0
		rangoIzY = 0
		face_cascade = cv2.CascadeClassifier('lbpcascade_frontalface.xml')
		img = cv2.imread('input/' + foto)
		height, width = img.shape[:2]
		intervalo = width/30#200
		gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
		faces = face_cascade.detectMultiScale(gray, 1.3, 5)#1.3,1.05
		if (len(faces) ==1):
			for (x,y,w,h) in faces:
				bordes[0] = rangoIzX = x-160
				if(y -18 < 0):
					bordes[1] = rangoIzY = 0;
				else:
					bordes[1] = rangoIzY = y - 18;
				bordes[2] = rangoDerX = x+w*2
				bordes[3] = rangoDerY = y+h
				#cv2.rectangle(img,(x-160,y),(x+w*2,y+h),(255,0,0),2)
				roi_gray = gray[y:y+h, x:x+w]
				roi_color = img[y:y+h, x:x+w]
			"""for x in range(0,width,intervalo):
				if ((x<rangoIzX and x+(intervalo/1.4) < rangoIzX) or (x > rangoDerX and x-(intervalo/2) > rangoDerX )):
					cv2.line(img,(x,0),(x,height),(0,255,255),5)
				else:
					cv2.line(img,(x,0),(x,height),(0,0,255),5)"""
		#cv2.imwrite("output/Copia" + foto,img)
		return bordes

	def Pizarron(self,foto):
		#imagen = cv2.imread('input/' + foto)
		imagen = cv2.imread('input/'+foto)
		#rangoArriba = np.array([65, 65, 255])
		#rangoAbajo = np.array([0, 0, 200])
		#BGR LOS DE ABAJO SON
		rangoArriba = np.array([255,255,90])
		rangoAbajo = np.array([0, 0, 0])
		#rangoArriba = np.array([104,255,131])
		#rangoAbajo = np.array([0, 55, 0])
		mask = cv2.inRange(imagen, rangoAbajo, rangoArriba)
		(_, cnts, _) = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL,
		cv2.CHAIN_APPROX_SIMPLE)
		if(len(cnts) != 0):
			c = max(cnts, key=cv2.contourArea)
			peri = cv2.arcLength(c, True)
			xs = np.arange(4)
			ys = np.arange(4)
			approx = cv2.approxPolyDP(c, 0.05 * peri, True)
			if (len(approx)>=4):
				for x in range(4):
					xs[x] = approx[x][0][0]
					ys[x] = approx[x][0][1]
				imagenSalida = imagen[np.amin(ys):np.amax(ys),np.amin(xs):np.amax(xs)]
				#cv2.drawContours(imagen, [approx], -1, (0, 0, 255), 4)DIBUJA EL CUADRO ROJO
				cv2.imwrite("temp/" + foto,imagenSalida)

	def Catedratico(self,foto,persona,firstTime,bordes,numFoto):
		imagen = cv2.imread('input/'+foto)
		nombreFoto = ""
		#tamanoX = 1024
		#tamanoY = 576
		tamanoX = 2592
		tamanoY = 1994
		intervaloY = 4
		intervaloX = 8
		#y,x
		if(firstTime):
			nombreFoto = foto
			img = np.zeros([tamanoY,tamanoX,3],dtype = np.uint8)
		else:
			nombreFoto = os.listdir("temp")[0] + ""
			img = cv2.imread('temp/' + nombreFoto)


		punto1 = [persona[0],persona[1]]
		#576 es el alto de la foto esto va a cambiar
		punto2 = [persona[2],tamanoY]

		#for y in xrange(592,-0,-16):
		for y in xrange(0,tamanoY,intervaloY):
			for x in xrange (0,tamanoX,intervaloX):
				rangoCortarX = [x,y]
				rangoCortarY = [x+intervaloX,y-intervaloY]
				if( y+intervaloY < punto1[1]):
					pedazo = imagen[y:y+intervaloY,x:x+intervaloX]
					img[y:y+intervaloY,x:x+intervaloX] = pedazo
				elif( x+intervaloX <= punto1[0] or x > punto2[0] ):
					pedazo = imagen[y:y+intervaloY,x:x+intervaloX]
					img[y:y+intervaloY,x:x+intervaloX] = pedazo
		if(numFoto >= 2):
			cv2.imwrite("temp/" + 'numFoto' + nombreFoto,img)
		else:
			cv2.imwrite("temp/" + nombreFoto,img)