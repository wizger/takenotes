"""Autor: Gerson Ivan Perez Dominguez
12002764"""
from Detect import Detect
from Close import Close
import os.path
import time
import numpy as np
import cv2
import zipfile
import datetime
import glob



def Init():
	if not os.path.exists("input"):
		os.makedirs("input")
	if not os.path.exists("output"):
		os.makedirs("output")
	if not os.path.exists("temp"):
		os.makedirs("temp")


#main
Init()

detect = Detect()#Este objeto es el que detecta a la persona y corta
close = Close()#Este objeto esel que convierte en zip y envia a los estudiantes
firstTime = True
bordesAnt = np.arange(4)
#De aqui se envian los correos a la lista
tooArr = ["gerson1093@gmail.com","gerson93@galileo.edu","TakeNotesUG@gmail.com"]#Este arreglo va a tener los correos de todos los estudiantes a los cuales se les va a enviar
#Aqui termina los envios de mensajes

timeDay = datetime.datetime.now()#Con esta varible se puede conocer hour,minute,second
timepoInicio = timeDay.strftime("%A %B %H:%M")#se va a inicializar con un web service 
timepoFinal = "Friday November 12:59"#se va a inicializar con un web service 
numFoto = 0



while(True):
	timeDay = datetime.datetime.now()
	timepoInicio = timeDay.strftime("%A %B %H:%M")
	#print timepoInicio
	if(timepoInicio == timepoFinal):
		zipf = zipfile.ZipFile('Clase.zip', 'w')#cambiar el nombre del zip al de la clase cuando se tenga el web service
		close.zip('temp/', zipf)
		zipf.close()
		close.send_email("TakeNotesUG@gmail.com","nosequeponer",tooArr,"Fotos","Fotos")

		files = glob.glob('temp/*')
		#break
		for f in files:
			os.remove(f)
		cont = 0
		firstTime = True
		break
	if len(os.listdir("input")) >0:
		bordes = detect.Persona(os.listdir("input")[0])
		if((bordes[0] != 0)  and (bordes[1] != 1) and (bordes[2] != 2) and (bordes[3] != 3)):
			#detect.Pizarron(os.listdir("input")[0])
			if(bordesAnt[0] != bordes[0] and bordesAnt[1] != bordes[1] and bordesAnt[2] != bordes[2] and bordesAnt[3] != bordes[3]):
				detect.Catedratico(os.listdir("input")[0],bordes,firstTime,bordes,numFoto)
				numFoto +=1
			if(firstTime):
				firstTime = False
			bordesAnt = bordes

		os.remove("input/" + os.listdir("input")[0])
		#falta unir analizar la imagen
	time.sleep(1)

